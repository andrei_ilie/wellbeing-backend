const mongoose = require('../data_access/db');

const userSchema = new mongoose.Schema({
  _id: { type: mongoose.Types.ObjectId,
    default: function genUUID() {
      mongoose.Types.ObjectId()
    }},
    email: {
      type: String, 
      required: true
    },
    password: {
      type: String, 
      required: true
    },
    name: {
      type: String,
      required: true
    }
});

const userModel = mongoose.model('User', userSchema);

module.exports = userModel;