const mongoose = require("../data_access/db");
var SchemaTypes = mongoose.Schema.Types;

const emotionSchema = new mongoose.Schema({
    day: {
        type: Date,
        default: new Date().setUTCHours(0, 0 ,0, 0)
    },
    emotions: {
        type: Array
    },
    userId: String
});

const model = mongoose.model("Emotion", emotionSchema);

module.exports = model;