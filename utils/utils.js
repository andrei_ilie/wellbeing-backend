const authorizationService = require('../bussiness/authorizationService')
const { secretKey } = require('../config/config');

exports.getUserInfo = function(userToken, callback) {
    authorizationService.verifyToken(userToken, secretKey, function(err, decode) {
        if(err) {
            console.log(err);

            return callback(err);
        }
  
        callback(null, decode.id);
    });
};