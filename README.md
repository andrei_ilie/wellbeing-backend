# wellbeing-backend

1. npm install
2. npm start

REQUEST EXAMPLE:

POST: http://wellbeing-1.herokuapp.com/send_photo
body: {
    photo: <base64binary||STRING>
}
HEADERS: { token: <USER_TOKEN> }

RESPONSE EXAMPLE:
{ status: 0 } --- success
{ status: 1 } --- failure

GET: http://wellbeing-1.herokuapp.com/get_metrics
HEADERS: { token: <USER_TOKEN> }

SUCCESS RESPONSE EXAMPLE:
{
    "status": 0,
    "metrics": [
        {
            "day": "1572739200000",
            "emotions": [ "DISGUSTED", "ANGRY", "SAD", "SURPRISED", "CALM", "CONFUSED", "FEAR", "HAPPY"],
            "values": [ 0.01, 0.01, 0.03, 0.13, 0.01, 0.03, 0.02, 99.77]
        }
    ]
}

{ status: 1 } --- failure


