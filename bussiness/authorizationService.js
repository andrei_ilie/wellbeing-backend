const jwt = require('jsonwebtoken');

class authorizationService {
    createToken(body, secretKey) {
        const payload = {
            username: body.email,
            id: body._id
        };

        const token = jwt.sign(payload, secretKey, { expiresIn: '2h' });
        
        return token;
    }

    verifyToken(token, secretKey, callback){
        jwt.verify(token, secretKey, callback);
    }
};

module.exports = new authorizationService();