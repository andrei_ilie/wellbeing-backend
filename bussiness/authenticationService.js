const userModel = require('../models/userModel');
const crypto = require('crypto');
const mongoose = require('../data_access/db');
const authorizationService = require('../bussiness/authorizationService');
const { secretKey } = require('../config/config');

class authenticationService {
    register(data, callback) {
        data.password = crypto.createHash('sha256').update(data.password).digest('base64');
        data._id = mongoose.Types.ObjectId();

        const user = new userModel(data);  
            
        user.save((err, data) => {
            if(err) {
                return callback(err);
            }

            callback(null, data);
        });
    }

    login(data, callback) {
        userModel.findOne({
            email: data.email,
            password: crypto.createHash('sha256').update(data.password).digest('base64')
        }, (err, user) => {
            if(err) {
                return callback(err);
            }

            if(user === null) {
                return callback(new Error('User not found!'));
            }

            callback(null, {
                email: user.email,
                name: user.name,
                token: authorizationService.createToken(user, secretKey)
            });
        });
    }
};

module.exports = new authenticationService();