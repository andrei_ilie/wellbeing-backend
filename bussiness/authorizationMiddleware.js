const { secretKey } = require('../config/config');
const authorizationService = require('./authorizationService');

module.exports = function(req, res, next) {
    const token = req.get("token");

    authorizationService.verifyToken(token, secretKey, function(err, decode) {
        if(err) {
            const response = {status: 401, message: "Unauthorized"};
            return next(response);
        }

        req.decoded = decode;
        next();
    });
};