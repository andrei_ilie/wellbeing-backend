var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser')

var sendPhoto = require('./routes/send_photo');
var getMetrics = require('./routes/get_metrics');
var getCategoryMetrics = require('./routes/getCategoryMetrics');
var recommandations = require('./routes/recommandations');
var authentication = require('./routes/authentication');
var authorization = require('./routes/authorizationApi');

var app = express();

app.use(logger('dev'));
app.use(cors());
// app.use(express.json());
// app.use(express.urlencoded({ extended: false, limit: Number.POSITIVE_INFINITY }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false, limit: Number.POSITIVE_INFINITY }));

// parse application/json
app.use(bodyParser.json({limit: Number.POSITIVE_INFINITY }));

app.use('/send_photo', sendPhoto);
app.use('/get_metrics', getMetrics);
app.use('/get_category_metrics', getCategoryMetrics);
app.use('/recommandations', recommandations);
app.use('/', authentication);
app.use('/', authorization);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(err.status || 500);
  res.json({ status: 1 });
  return next();
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({ error: err });
});

module.exports = app;
