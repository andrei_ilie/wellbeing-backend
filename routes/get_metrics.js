const express = require('express');
const router = express.Router();
const async = require('async');
const EmotionModel = require('../models/emotionModel');
const { getUserInfo } = require('../utils/utils');

router.get('/', (req, res, next) => {
    let metrics = {};
    let response = [];
    const userToken = req.get('token');
    
    async.waterfall([
        (cb) => {
            getUserInfo(userToken, cb)
        },
        (userId, cb) => {
            EmotionModel.find({ userId: userId })
                .select({ emotions: 1, day: 1, _id: 0 }).then((data) => {

                for(let i = 0; i < data.length; i++) {
                    let timestamp = data[i].day.getTime();

                    if(!metrics.hasOwnProperty(timestamp)) {
                        metrics[timestamp] = {};
                    }

                    if (data[i].emotions) {
                        data[i].emotions.forEach(emotion => {
                            if (!metrics[timestamp][emotion.Type]) {
                                metrics[timestamp][emotion.Type] = [];
                            }
    
                            metrics[timestamp][emotion.Type].push(emotion.Confidence);
                        });
                    }
                }

                Object.keys(metrics).forEach((day) => {
                    let keys = Object.keys(metrics[day]);
                    let values = [];

                    keys.forEach(key => {
                        values.push(parseFloat((metrics[day][key]
                            .reduce((a, b) => a + b) / metrics[day][key].length).toFixed(2)));
                    });

                    response.push({
                        day: day,
                        emotions: keys,
                        values: values
                    });
                }); 

                return cb(null, {
                    status: 0,
                    metrics: response
                });
            })
            .catch((err) => {
                console.log('Database error! Something went wrong!');

                return cb(err);
            }); 
        }
    ], (err, response) => {
        console.log(err);
        if (err) {
            res.statusCode = 401;
            return res.json({ status: 1 });
        }

        res.json(response);
    });
});

module.exports = router;