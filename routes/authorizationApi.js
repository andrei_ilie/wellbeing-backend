const authorizationService = require('../bussiness/authorizationService')
const express = require('express');
const router = express.Router();
const { secretKey } = require('../config/config');
const path = '/api/authorization/';

router.post(path, function(req, res) {
    const token = authorizationService.createToken(req.body, secretKey);

    res.json({
        token: token
    });
});

router.post(path + 'verify', function(req, res) {
    const token = req.body.token;

    authorizationService.verifyToken(token, secretKey, function(err, decode) {
        if(err) {
            return res.json({ status: 1 });
        }

        res.json({ status: 0 });
    });
});

module.exports = router;