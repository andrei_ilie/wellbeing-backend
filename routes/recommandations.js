const express = require('express');
const router = express.Router();
const emotionModel = require('../models/emotionModel');
const quotes = require('../resources/quotes.json');
const request = require('request');
const authorizationMiddleware = require('../bussiness/authorizationMiddleware');

const negativeEmotions = ["SAD", "FEAR", "ANGRY", "DISGUSTED"];
const positiveEmotions = ["HAPPY", "CALM"];

router.get('/', authorizationMiddleware, function (req, res) {
    const id = req.decoded.id;
    emotionModel.find({'userId':id}, function (err, data) {
        if (err) {
            res.end();
        }

        let positiveScore = 0;
        let negativeScore = 0;

        data.forEach((img) => {
            if(img.emotions != null) {
                img.emotions.forEach((e) => {
                    if (negativeEmotions.includes(e.Type)) {
                        if(e.Type == "SAD") {
                            negativeScore += e.Confidence;
                        }
                        negativeScore += e.Confidence;
                    } else if (positiveEmotions.includes(e.Type)) {
                        positiveScore += e.Confidence;
                        if(e.Type == "HAPPY") {
                            positiveScore += e.Confidence;
                        }
                    }
                });
            }
        });

        negativeScore /= (negativeEmotions.length + 1) * data.length;
        positiveScore /= (positiveEmotions.length + 1) * data.length;

        getNotificationMessage(positiveScore - negativeScore, function(msg){
            res.json(msg);    
        });
    });
});

var getNotificationMessage = function(diff, callback){
    const differenceTreshold = 15;
    let strategies;

    if(Math.abs(diff) < differenceTreshold) {
        strategies = [jokeStrategy, randomMessageStrategy, dogPhotoStrategy, catPhotoStrategy];
    } else if (diff < 0) {
        strategies = [randomMessageStrategy, dogPhotoStrategy, catPhotoStrategy]
    } else {
        strategies = [jokeStrategy, positiveMessageStrategy, dogPhotoStrategy, catPhotoStrategy];
    }

    strategy = strategies[Math.floor(Math.random() * strategies.length)];

    strategy(callback);
};

var randomMessageStrategy = function(callback) {
    const title = "Don't like to see you sad";
    const quote = quotes[Math.floor(Math.random() * quotes.length)];
    const resObj = {
        title: title,
        message: quote
    };
    callback(resObj);
   

};

var jokeStrategy = function(callback) {
    const apiUrl = "https://icanhazdadjoke.com/";

    request(apiUrl, { json: true }, (err, res, body) => {
        if(err){
            res.json(err);
        }

        const resObj = {
            title: "Time for a little joke",
            message: body.joke
        };
        callback(resObj);
    });
};

var dogPhotoStrategy = function(callback) {
    const apiUrl = "https://dog.ceo/api/breeds/image/random";

    request(apiUrl, { json: true }, (err, res, body) => {
        if(err){
            res.json(err);
        }

        const resObj = {
            title: "Here's a puppy photo for you",
            message: body.message
        };

        callback(resObj);
    });
};

var positiveMessageStrategy = function(callback) {
    const title = "Hello there!";
    const positiveQuote = "Glad to see you feeling well today";
    const resObj = {
        title: title,
        message: positiveQuote
    };

    callback(resObj);
}

var catPhotoStrategy = function(callback) {
    const apiUrl = "https://api.thecatapi.com/v1/images/search";

    request(apiUrl, { json: true }, (err, res, body) => {
        if(err){
            res.json(err);
        }

        const resObj = {
            title: "Here's a cat photo for you",
            message: body[0].url
        };

        callback(resObj);
    });
}

module.exports = router;