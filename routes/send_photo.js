const express = require('express');
const router = express.Router();
const async = require('async');
const AWS = require('aws-sdk');

const { awsOptions } = require('../config/options');
// const photo_mock = require('../utils/utils').photo;
const emotionModel = require('../models/emotionModel');
const { getUserInfo } = require('../utils/utils');

const awsClient = new AWS.Rekognition(awsOptions);

function getEmotions(awsClient, params, userId, callback) {
  awsClient.detectFaces(params, function (err, data) {
    if (err) {
      console.log(err);

      return callback(err);
    }

    callback(null, data.FaceDetails[0] != null ? data.FaceDetails[0].Emotions : null, userId);
  });
}

function saveData(emotions, userId, EmotionModel, callback) {
  emotion = new EmotionModel({
    userId: userId,
    emotions: emotions
  });

  emotion.save((err) => {
    if (err) {
      return callback(err);
    }

    callback(null, { status: 0 });
  });
}

router.post('/', function (req, res, next) {
  const userToken = req.get('token');
  const { photo } = req.body;
  const params = {
    Image: {
      Bytes: Buffer.from(photo, 'base64')
    },
    Attributes: ['ALL']
  };

  async.waterfall([
    (cb) => {
      getUserInfo(userToken, cb)
    },
    (userId, cb) => {
      getEmotions(awsClient, params, userId, cb)
    },
    (emotions, userId, cb) => {
      saveData(emotions, userId, emotionModel, cb)
    }
  ], (error, response) => {
    if (error) {
      console.log(error);

      res.statusCode = 401;
      return res.json({ status: 1 });
    }

    res.json(response);
  });
});

module.exports = router;
