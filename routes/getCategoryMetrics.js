const express = require('express');
const router = express.Router();
const async = require('async');
const EmotionModel = require('../models/emotionModel');
const { getUserInfo } = require('../utils/utils');

router.get('/', (req, res, next) => {
    let metrics = {};
    let response = [];
    let responseObject = [];
    let days = [];
    let scores = {};
    const userToken = req.get('token');
    
    async.waterfall([
        (cb) => {
            getUserInfo(userToken, cb)
        },
        (userId, cb) => {
            EmotionModel.find({ userId: userId })
                .select({ emotions: 1, day: 1, _id: 0 }).then((data) => {

                for(let i = 0; i < data.length; i++) {
                    let timestamp = data[i].day.getTime();

                    if(!metrics.hasOwnProperty(timestamp)) {
                        metrics[timestamp] = {};
                    }

                    if (data[i].emotions) {
                        data[i].emotions.forEach(emotion => {
                            if (!metrics[timestamp][emotion.Type]) {
                                metrics[timestamp][emotion.Type] = [];
                            }
    
                            metrics[timestamp][emotion.Type].push(emotion.Confidence);
                        });
                    }
                }

                Object.keys(metrics).forEach((day) => {
                    let keys = Object.keys(metrics[day]);
                    let values = [];

                    keys.forEach(key => {
                        values.push(parseFloat((metrics[day][key]
                            .reduce((a, b) => a + b) / metrics[day][key].length).toFixed(2)));
                    });

                    response.push({
                        day: day,
                        emotions: keys,
                        values: values
                    });
                });

                for(let i = 0; i < response.length; i++) {
                    days.push(response[i].day);
                }

                for(let i = 0; i < response[0].emotions.length; i++) {
                    if (!scores.hasOwnProperty(response[0].emotions[i])) {
                        scores[response[0].emotions[i]] = [];
                    }

                    for(let j = 0; j < response.length; j++) {
                        scores[response[0].emotions[i]].push(response[j].values[i]);
                    }
                }

                Object.keys(scores).forEach(category => {
                    responseObject.push({
                        category: category,
                        subjects: days,
                        scores: scores[category]
                    });
                });

                return cb(null, responseObject);
            })
            .catch((err) => {
                console.log('Database error! Something went wrong!');

                return cb(err);
            }); 
        }
    ], (err, response) => {
        console.log(err);
        if (err) {
            res.statusCode = 400;
            return res.json({ status: 1 });
        }

        res.json(response);
    });
});

module.exports = router;