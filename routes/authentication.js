const authenticationService = require('../bussiness/authenticationService');
const express = require('express');
const router = express.Router();
const path = '/api/users/';

router.post(path + 'register', function (req, res) {
    authenticationService.register(req.body, function(err, user) {
        if(err) {
            console.log(err);

            res.statusCode = 500;
            return res.json(err);
        }

        res.json({ status: 0 });
    });
});

router.post(path + 'login', function (req, res) {
    authenticationService.login(req.body, function(err, user) {
        if(err) {
            console.log(err);

            res.statusCode = 404;
            return res.json(err);
        }

        res.json(user);
    });
});

module.exports = router;